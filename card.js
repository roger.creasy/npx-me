#!/usr/bin/env node

'use strict'

const boxen = require("boxen");
const chalk = require("chalk");
const inquirer = require("inquirer");
const clear = require("clear");
const open = require("open");

// clear the terminal before showing the npx card
clear()

const prompt = inquirer.createPromptModule();

const questions = [
    {
        type: "list",
        name: "action",
        message: "What do you want to do?",
        choices: [
            {
                // Use chalk to style headers
                name: `Send me an ${chalk.bold("email")}?`,
                value: () => {
                    open("mailto:roger@rogercreasy.com");
                    console.log("\nLooking forward to hearing your message and replying to you!\n");
                }
            },
            {
                name: "Exit",
                value: () => {
                    console.log("Good bye, have a nice day!\n");
                }
            }
        ]
    }
];


const data = {
    name: chalk.bold.green("                                            Roger Creasy"),
    handle: chalk.white("@rogercreasy"),
    fact: chalk.hex('#FF6262')('FULL stack developer, advocate for Opensource and Linux'),
    twitter: chalk.hex('#00A1D9')("https://twitter.com/RogerCreasy"),
    github: chalk.hex('#787878')("https://github.com/RogerCreasy"),
    website: chalk.hex('#00AB9E')("https://RogerCreasy.com"),
    ham: chalk.green("KN4OJN"),
    npx: chalk.hex('#A1AB00')("npx rogercreasy"),

    labelFact: chalk.hex('#B10000').bold("                About:"),
    labelTwitter: chalk.hex('#629DFF').bold("              Twitter:"),
    labelGitHub: chalk.hex('#9E9E9E').bold("               GitHub:"),
    labelWebsite: chalk.hex('#59FFC8').bold("              Website:"),
    labelHam: chalk.hex('#59FFC8').bold("        Amateur Radio:"),
    labelCard: chalk.hex('#FFF976').bold("                 Card:")
};


const me = boxen(
    [
        `${data.name}`,
        ``,
        `${data.labelFact}  ${data.fact}`,
        ``,
        `${data.labelTwitter}  ${data.twitter}`,
        `${data.labelGitHub}  ${data.github}`,
        `${data.labelWebsite}  ${data.website}`,
        `${data.labelHam}  ${data.ham}`,
        ``,
        `${data.labelCard}  ${data.npx}`,
        ``,
        `${chalk.bold(
            "Howdy! I am Roger Creasy, a fullstack developer. "
        )}`,
        `${chalk.bold("I work as a true fullstack develper, frontend, backend, database, and server management. ")}`,
        `${chalk.bold(
            "I am a conference speaker who has given talks on Linux and Linux Server security. I also build and manage"
        )}`,
        `${chalk.bold(
            "websites for small businesses. If you have a need for a site or a speaker, send me an email."
        )}`
    ].join("\n"),
    {
        margin: 1,
        float: 'center',
        padding: 1,
        borderStyle: "single",
        borderColor: "blue"
    }
);

// Show the boxen
console.log(me);

prompt(questions).then(answer => answer.action());
